class Puerd::Attribute
  property name : String
  property? nullable = true
  property? unique = false
  property? primary_key = false
  property? foreign_key = false

  def initialize(@name, @nullable = true, @unique = false)
  end

  def to_s(io)
    if primary_key?
      io << "* "
    elsif foreign_key?
      io << "# "
    end
    io << name
  end
end
