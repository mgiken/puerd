require "./table"

class Puerd::Relation
  property r_table : Table
  property l_table : Table
  property cardinality : String

  def initialize(@l_table, l_attr : String, @r_table, r_attr : String)
    l = l_table.attr_by_name[l_attr]
    r = r_table.attr_by_name[r_attr]

    @cardinality = String.build do |s|
      s << "|"
      s << (r.nullable? ? "o" : "|")
      s << "--"
      s << (r.unique? ? (l.nullable? ? "o|" : "||") : "{")
    end
  end

  def to_s(io)
    io << l_table.schema << "." << l_table.name
    io << " " << cardinality << " "
    io << r_table.schema << "." << r_table.name
  end
end
