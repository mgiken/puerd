require "ecr"

require "./attribute"

class Puerd::Table
  property schema : String
  property name : String
  property attr_by_name = Hash(String, Attribute).new

  def initialize(@schema, @name)
  end

  def attrs
    attr_by_name.values
  end

  ECR.def_to_s "#{__DIR__}/table.ecr"
end
