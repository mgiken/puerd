require "ecr"

require "./attribute"
require "./relation"
require "./table"

class Puerd::ERD
  getter relations = Array(Relation).new
  getter tables_by_schema = Hash(String, Hash(String, Table)).new { |h, k| h[k] = Hash(String, Table).new }

  def initialize
  end

  def self.load(input : String | IO)
    new.tap(&.load(input))
  end

  ECR.def_to_s "#{__DIR__}/erd.ecr"

  protected def load(filename : String)
    File.open(filename) do |io|
      load(io)
    end
  end

  protected def load(io : IO)
    io.each_line do |line|
      next if line.blank?
      next if line.starts_with?("--")
      next if parse_create_table(line)
      next if parse_alter_table(line)
      next if parse_unique_index(line)
    end
  end

  @create_table : Table?

  private def parse_create_table(line)
    if m = line.match(/^CREATE TABLE (\S+)\.(\S+) \($/)
      table = Table.new(m[1], m[2])
      @create_table = @tables_by_schema[table.schema][table.name] = table
      return true
    end

    return false unless @create_table

    if line == ");"
      @create_table = nil
      return true
    end

    return true if line.starts_with?("    CONSTRAINT ")

    attr = Attribute.new(line.split(2).first, !line.match(/ NOT NULL/))
    @create_table.not_nil!.attr_by_name[attr.name] = attr
    true
  end

  @alter_table : Table?

  private def parse_alter_table(line)
    if m = line.match(/^ALTER TABLE ONLY (\S+)\.(\S+)$/)
      @alter_table = tables_by_schema[m[1]][m[2]]?
      return true
    end

    return false unless @alter_table

    if m = line.match(/ADD CONSTRAINT .+ PRIMARY KEY \((.+)\);$/)
      pkeys = m[1].split(", ")
      pkeys.each do |name|
        attr = @alter_table.not_nil!.attr_by_name[name]
        attr.primary_key = true
        attr.nullable = false
        attr.unique = pkeys.size == 1
      end
      return true
    end

    if m = line.match(/ADD CONSTRAINT .+ FOREIGN KEY \((\S+)\) REFERENCES (\S+)\.(\S+)\((\S+)\)/)
      @alter_table.not_nil!.attr_by_name[m[1]].foreign_key = true
      @relations << Relation.new(tables_by_schema[m[2]][m[3]], m[4], @alter_table.not_nil!, m[1])
      return true
    end

    if m = line.match(/ADD CONSTRAINT .+ UNIQUE \((\S+)\);/)
      if it = @alter_table.not_nil!.attr_by_name[m[1]]?
        it.unique = true
      end
      return true
    end
  end

  private def parse_unique_index(line)
    if m = line.match(/CREATE UNIQUE INDEX .+ ON (\S+)\.(\S+) USING .+ \((\S+)\);/)
      if it = tables_by_schema[m[1]][m[2]].attr_by_name[m[3]]?
        it.unique = true
      end
      return true
    end
  end
end
