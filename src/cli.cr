require "cliche"

require "./puerd"

main do
  description "convert PostgreSQL schema dump file to ER diagram in PlantUML"

  option input = "", "i|input FILE|read from FILE instead of stdin"
  option output = "", "o|output FILE|write to FILE instead of stdout"

  erd = Puerd::ERD.load(input.blank? ? STDIN : input)

  if output.blank?
    print(erd)
  else
    File.write(output, erd)
  end
end
