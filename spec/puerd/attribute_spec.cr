require "../spec_helper"

describe Puerd::Attribute do
  describe "#to_s" do
    context "when primary key" do
      it "returns attribute name with prefix `*`" do
        attr = Puerd::Attribute.new("attribute")
        attr.primary_key = true
        attr.to_s.should eq "* attribute"
      end
    end

    context "when foreign key" do
      it "returns attribute name with prefix `#`" do
        attr = Puerd::Attribute.new("attribute")
        attr.foreign_key = true
        attr.to_s.should eq "# attribute"
      end
    end

    context "when normal" do
      it "returns attribute name" do
        attr = Puerd::Attribute.new("attribute")
        attr.to_s.should eq "attribute"
      end
    end
  end
end
