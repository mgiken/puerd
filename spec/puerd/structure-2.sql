--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Debian 11.5-3.pgdg90+1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;


--
-- Name: foo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.foo (
    foo_id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- Name: bar; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bar (
    bar_id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- Name: baz; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.baz (
    bar_id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    name text NOT NULL,
);


--
-- Name: foobar; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.foobar (
    foo_id uuid NOT NULL,
    bar_id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
);


--
-- Name: foo foo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.foo
    ADD CONSTRAINT foo_pkey PRIMARY KEY (foo_id);


--
-- Name: bar bar_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bar
    ADD CONSTRAINT bar_pkey PRIMARY KEY (bar_id);


--
-- Name: baz baz_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.baz
    ADD CONSTRAINT baz_pkey PRIMARY KEY (bar_id);


--
-- Name: foobar foobar_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.foobar
    ADD CONSTRAINT foobar_pkey PRIMARY KEY (foo_id, bar_id);


--
-- Name: baz baz_bar_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.baz
    ADD CONSTRAINT baz_bar_id_fkey FOREIGN KEY (bar_id) REFERENCES public.bar(bar_id) ON DELETE CASCADE;


--
-- Name: foobar foobar_bar_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.foobar
    ADD CONSTRAINT foobar_bar_id_fkey FOREIGN KEY (bar_id) REFERENCES public.bar(bar_id) ON DELETE CASCADE;


--
-- Name: foobar foobar_foo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.foobar
    ADD CONSTRAINT foobar_foo_id_fkey FOREIGN KEY (foo_id) REFERENCES public.foo(foo_id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

