require "../spec_helper"

describe Puerd::ERD do
  describe "#to_s" do
    it "returns PlantUML ERD" do
      erd = Puerd::ERD.load("#{__DIR__}/structure-1.sql")
      erd.to_s.should eq <<-EOS
        @startuml ERD

        hide circle
        hide empty members

        skinparam linetype ortho
        skinparam monochrome true
        skinparam roundcorner 6
        skinparam shadowing false
        skinparam packagestyle rectangle


        package "public" {

          entity "bar" as public.bar {
            * bar_id
            # foo_id
          }

          entity "foo" as public.foo {
            * foo_id
          }

        }


        public.foo ||--{ public.bar


        @enduml

        EOS
    end

    it "returns PlantUML ERD" do
      erd = Puerd::ERD.load("#{__DIR__}/structure-2.sql")
      erd.to_s.should eq <<-EOS
        @startuml ERD

        hide circle
        hide empty members

        skinparam linetype ortho
        skinparam monochrome true
        skinparam roundcorner 6
        skinparam shadowing false
        skinparam packagestyle rectangle


        package "public" {

          entity "foo" as public.foo {
            * foo_id
            created_at
          }

          entity "bar" as public.bar {
            * bar_id
            created_at
          }

          entity "baz" as public.baz {
            * bar_id
            created_at
            updated_at
            name
          }

          entity "foobar" as public.foobar {
            * foo_id
            * bar_id
            created_at
            updated_at
          }

        }


        public.bar ||--|| public.baz
        public.bar ||--{ public.foobar
        public.foo ||--{ public.foobar


        @enduml

        EOS
    end
  end
end
