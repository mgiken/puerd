require "../spec_helper"

describe Puerd::Table do
  describe "#to_s" do
    it "returns entity definition" do
      table = Puerd::Table.new("schema", "table")
      table.attr_by_name["attribute"] = Puerd::Attribute.new("attribute")
      table.to_s.should eq <<-EOS
          entity "table" as schema.table {
            attribute
          }

        EOS
    end
  end
end
