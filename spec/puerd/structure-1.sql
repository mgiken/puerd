--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7 (Debian 10.7-1.pgdg90+1)
-- Dumped by pg_dump version 11.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bar; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bar (
    bar_id integer NOT NULL,
    foo_id integer NOT NULL
    CONSTRAINT bar_check CHECK (foo_id > 1)
);


--
-- Name: bar_bar_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bar_bar_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bar_bar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bar_bar_id_seq OWNED BY public.bar.bar_id;


--
-- Name: foo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.foo (
    foo_id integer NOT NULL
);


--
-- Name: foo_foo_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.foo_foo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: foo_foo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.foo_foo_id_seq OWNED BY public.foo.foo_id;


--
-- Name: bar bar_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bar ALTER COLUMN bar_id SET DEFAULT nextval('public.bar_bar_id_seq'::regclass);


--
-- Name: foo foo_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.foo ALTER COLUMN foo_id SET DEFAULT nextval('public.foo_foo_id_seq'::regclass);


--
-- Name: bar bar_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bar
    ADD CONSTRAINT bar_pkey PRIMARY KEY (bar_id);


--
-- Name: foo foo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.foo
    ADD CONSTRAINT foo_pkey PRIMARY KEY (foo_id);


--
-- Name: bar bar_foo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bar
    ADD CONSTRAINT bar_foo_id_fkey FOREIGN KEY (foo_id) REFERENCES public.foo(foo_id);


--
-- PostgreSQL database dump complete
--

