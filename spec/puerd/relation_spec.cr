require "../spec_helper"

describe Puerd::Relation do
  describe "#to_s" do
    context "when left is nullable and right is nullable, not unique" do
      it "returns relation with cardinality `|o--{`" do
        l = Puerd::Table.new("public", "l_table")
        l.attr_by_name["l_attr"] = Puerd::Attribute.new("l_attr", true, false)
        r = Puerd::Table.new("public", "r_table")
        r.attr_by_name["r_attr"] = Puerd::Attribute.new("r_attr", true, false)

        rel = Puerd::Relation.new(l, "l_attr", r, "r_attr")
        rel.to_s.should eq "public.l_table |o--{ public.r_table"
      end
    end

    context "when left is nullable and right is not nullable, not unique" do
      it "returns relation with cardinality `||--{`" do
        l = Puerd::Table.new("public", "l_table")
        l.attr_by_name["l_attr"] = Puerd::Attribute.new("l_attr", true, false)
        r = Puerd::Table.new("public", "r_table")
        r.attr_by_name["r_attr"] = Puerd::Attribute.new("r_attr", false, false)

        rel = Puerd::Relation.new(l, "l_attr", r, "r_attr")
        rel.to_s.should eq "public.l_table ||--{ public.r_table"
      end
    end

    context "when left is nullable and right is nullable, unique" do
      it "returns relation with cardinality `|o--o|`" do
        l = Puerd::Table.new("public", "l_table")
        l.attr_by_name["l_attr"] = Puerd::Attribute.new("l_attr", true, false)
        r = Puerd::Table.new("public", "r_table")
        r.attr_by_name["r_attr"] = Puerd::Attribute.new("r_attr", true, true)

        rel = Puerd::Relation.new(l, "l_attr", r, "r_attr")
        rel.to_s.should eq "public.l_table |o--o| public.r_table"
      end
    end

    context "when left is nullable and right is not nullable, unique" do
      it "returns relation with cardinality `||--o|`" do
        l = Puerd::Table.new("public", "l_table")
        l.attr_by_name["l_attr"] = Puerd::Attribute.new("l_attr", true, false)
        r = Puerd::Table.new("public", "r_table")
        r.attr_by_name["r_attr"] = Puerd::Attribute.new("r_attr", false, true)

        rel = Puerd::Relation.new(l, "l_attr", r, "r_attr")
        rel.to_s.should eq "public.l_table ||--o| public.r_table"
      end
    end

    context "when left is not nullable and right is nullable, not unique" do
      it "returns relation with cardinality `|o--{`" do
        l = Puerd::Table.new("public", "l_table")
        l.attr_by_name["l_attr"] = Puerd::Attribute.new("l_attr", false, false)
        r = Puerd::Table.new("public", "r_table")
        r.attr_by_name["r_attr"] = Puerd::Attribute.new("r_attr", true, false)

        rel = Puerd::Relation.new(l, "l_attr", r, "r_attr")
        rel.to_s.should eq "public.l_table |o--{ public.r_table"
      end
    end

    context "when left is not nullable and right is not nullable, not unique" do
      it "returns relation with cardinality `||--{`" do
        l = Puerd::Table.new("public", "l_table")
        l.attr_by_name["l_attr"] = Puerd::Attribute.new("l_attr", false, false)
        r = Puerd::Table.new("public", "r_table")
        r.attr_by_name["r_attr"] = Puerd::Attribute.new("r_attr", false, false)

        rel = Puerd::Relation.new(l, "l_attr", r, "r_attr")
        rel.to_s.should eq "public.l_table ||--{ public.r_table"
      end
    end

    context "when left is not nullable and right is nullable, unique" do
      it "returns relation with cardinality `|o--||`" do
        l = Puerd::Table.new("public", "l_table")
        l.attr_by_name["l_attr"] = Puerd::Attribute.new("l_attr", false, false)
        r = Puerd::Table.new("public", "r_table")
        r.attr_by_name["r_attr"] = Puerd::Attribute.new("r_attr", true, true)

        rel = Puerd::Relation.new(l, "l_attr", r, "r_attr")
        rel.to_s.should eq "public.l_table |o--|| public.r_table"
      end
    end

    context "when left is not nullable and right is not nullable, unique" do
      it "returns relation with cardinality `||--||`" do
        l = Puerd::Table.new("public", "l_table")
        l.attr_by_name["l_attr"] = Puerd::Attribute.new("l_attr", false, false)
        r = Puerd::Table.new("public", "r_table")
        r.attr_by_name["r_attr"] = Puerd::Attribute.new("r_attr", false, true)

        rel = Puerd::Relation.new(l, "l_attr", r, "r_attr")
        rel.to_s.should eq "public.l_table ||--|| public.r_table"
      end
    end
  end
end
